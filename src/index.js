//entry point for webpack
import React from 'react';
import ReactDOM from 'react-dom';
import Root from './Root';

import styles from './styles/global.css';

function startApp() {
  ReactDOM.render(<Root />, document.getElementById('app'));
}

function onDeviceReady() {
  startApp();
  if (window.StatusBar){
    StatusBar.hide();
  }
}

if (!__PHONEGAP__) {
  startApp();
} else {
  document.addEventListener('deviceready', () => {
    onDeviceReady();
  }, false);
}
