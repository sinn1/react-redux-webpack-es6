import React, {Component, PropTypes} from 'react';
import {Redirect, Router, Route, IndexRoute} from 'react-router';
import {Provider} from 'react-redux';
import {syncHistoryWithStore} from 'react-router-redux';
import { hashHistory, browserHistory } from 'react-router';
import { push } from 'react-router-redux';
import injectTapEventPlugin from 'react-tap-event-plugin';

import ApiClient from './helpers/ApiClient';
import configureStore from './redux';

import AppContainer from './views/AppContainer/AppContainer';
import Home from './views/Home/Home';
import AnotherView from './views/AnotherView/AnotherView';

injectTapEventPlugin();

let baseUrl = '';
let history = browserHistory;
if(__PHONEGAP__) {
  history = hashHistory;
  baseUrl = config.baseUrl;
}

const client = new ApiClient(baseUrl);

const store = configureStore(client, history);

export default class Root extends Component {

  render() {
    const enhancedHistory = syncHistoryWithStore(history, store, { selectLocationState: state => state.get('routing') });

    return (
      <Provider store={store}>
          <Router history={enhancedHistory}>
            <Route path="/" component={AppContainer}>
              <IndexRoute component={Home} />
              <Route path="anotherView" component={AnotherView} />
            </Route>
          </Router>
      </Provider>
    );
  }
};
