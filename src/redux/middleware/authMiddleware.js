import { push } from 'react-router-redux';

export default function authMiddleware() {
  return ({ dispatch, getState }) => {
    return next => action => {

      const { error, type, ...rest } = action;
      if (!error) {
        return next(action);
      }

      if (error.response) {
        if (error.response.status === 401) {
          //redirect to login
        }
      }

      next({ ...rest, error, type: type });
    };
  };
}
