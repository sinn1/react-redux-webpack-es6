import { compose, createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';
import rootReducer from './reducers';
import createClientMiddleware from './middleware/clientMiddleware';
import * as storage from 'redux-storage';
import createEngine from 'redux-storage-engine-localstorage';
import filter from 'redux-storage-decorator-filter';
import immutableMerger from 'redux-storage-merger-immutablejs';
import { routerMiddleware } from 'react-router-redux'
import authMiddleware from './middleware/authMiddleware';

export default function configureStore(client, history) {
  const reducer = storage.reducer(rootReducer, immutableMerger);
  let engine = createEngine('app-store');
  engine = filter(engine, [
    'auth',
  ]);
  const storageMiddleware = storage.createMiddleware(engine);

  const middleware = [
    routerMiddleware(history),
    thunkMiddleware,
    createClientMiddleware(client),
    authMiddleware(),
    loggerMiddleware({
      stateTransformer: (state) => {
        if (state.toJS) {
          return state.toJS();
        }

        return state;
      },
    }),
    storageMiddleware,
  ];

  const createPersistentStore = applyMiddleware(...middleware)(createStore);
  const store = createPersistentStore(reducer);

  const load = storage.createLoader(engine);
  load(store)
    .then((newState) => console.log('Loaded state: ', newState))
    .catch(() => console.log('Failed to load previous state'));

  return store;
}
