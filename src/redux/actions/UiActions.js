export const TOGGLE_LEFT_SIDE_PANEL_VISIBILITY = 'TOGGLE_LEFT_SIDE_PANEL_VISIBILITY';
export const TOGGLE_RIGHT_SIDE_PANEL_VISIBILITY = 'TOGGLE_RIGHT_SIDE_PANEL_VISIBILITY';
export const HANDLE_WINDOW_RESIZE_EVENT = 'HANDLE_WINDOW_RESIZE_EVENT';
export const UPDATE_KEYBOARD_HEIGHT = 'UPDATE_KEYBOARD_HEIGHT';

export function handleWindowResizeEvent(e) {
  return {
    type: HANDLE_WINDOW_RESIZE_EVENT,
    windowWidth: window.innerWidth,
    windowHeight: window.innerHeight,
  };
}

export function toggleRightSidePanelVisibility(isVisible) {
  return {
    type: TOGGLE_RIGHT_SIDE_PANEL_VISIBILITY,
    isVisible: isVisible,
  };
}

export function toggleLeftSidePanelVisibility(isVisible) {
  return {
    type: TOGGLE_LEFT_SIDE_PANEL_VISIBILITY,
    isVisible: isVisible,
  };
}

export function updateKeyboardHeight(height) {
  return {
    type: UPDATE_KEYBOARD_HEIGHT,
    height: height,
  }
}