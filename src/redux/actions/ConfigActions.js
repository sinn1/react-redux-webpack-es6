export const FETCH_CONFIG = 'ConfigActions/fetch_config';
export const FETCH_CONFIG_SUCCESS = 'ConfigActions/fetch_config_success';
export const FETCH_CONFIG_FAILED = 'ConfigActions/fetch_config_failed';

function shouldFetchConfig(configStore) {
  if (configStore.get('isFetching')) {
    return false;
  }

  return true;
}

export function fetchConfig() {
  return (dispatch, getState) => {
    if (shouldFetchConfig(getState().get('config'))) {
      return dispatch({
        types: [FETCH_CONFIG, FETCH_CONFIG_SUCCESS, FETCH_CONFIG_FAILED],
        promise: (client) => client.get(`/config`),
      });
    }
  };
}

