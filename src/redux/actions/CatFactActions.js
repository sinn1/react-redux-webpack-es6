export const FETCH_CAT_FACTS = 'CatFactActions/fetch_cat_facts';
export const FETCH_CAT_FACTS_SUCCESS = 'CatFactActions/fetch_cat_facts_success';
export const FETCH_CAT_FACTS_FAILED = 'CatFactActions/fetch_cat_facts_failed';

function shouldFetchCatFacts(store) {
	if (store.get('isFetching')) {
    return false;
  }

  return true;
}

export function fetchCatFacts() {
  return (dispatch, getState) => {
    if (shouldFetchCatFacts(getState().get('catFacts'))) {
      return dispatch({
        types: [FETCH_CAT_FACTS, FETCH_CAT_FACTS_SUCCESS, FETCH_CAT_FACTS_FAILED],
        promise: (client) => client.get('/facts', { params: { number: 5 } }),
      });
    }
  }
}
