import Immutable from 'immutable';

import {
  FETCH_CAT_FACTS,
  FETCH_CAT_FACTS_SUCCESS,
  FETCH_CAT_FACTS_FAILED,
} from '../actions/CatFactActions.js';

const DefaultState = Immutable.Record({
  data: null,
  isFetching: false,
  error: null,
  lastModified: null,
});

const initialState = new DefaultState;

function catFacts(state = initialState, action) {

  switch (action.type) {
    case FETCH_CAT_FACTS:
      return state.set('isFetching', true);

    case FETCH_CAT_FACTS_SUCCESS:
      return state.merge({
        isFetching: false,
        data: Immutable.fromJS(action.result),
        error: null,
        lastModified: action.receivedAt,
      });

    case FETCH_CAT_FACTS_FAILED:
      return state.merge({
        isFetching: false,
        error: action.error.message,
      });

    default:
      return state;
  }
}

export default catFacts;
