import { combineReducers } from 'redux-immutable';

import { routerReducer } from 'react-router-redux';
import config from './config';
import ui from './ui';
import catFacts from './catFacts';

const rootReducers = combineReducers({
  routing: routerReducer,
  config,
  ui,
  catFacts,
});

export default rootReducers;
