import Immutable from 'immutable';

import {
  FETCH_CONFIG,
  FETCH_CONFIG_SUCCESS,
  FETCH_CONFIG_FAILED,
} from '../actions/ConfigActions.js';

const DefaultState = Immutable.Record({
  data: null,
  isFetching: false,
  error: null,
  lastModified: null,
});

const initialState = new DefaultState;

function config(state = initialState, action) {

  switch (action.type) {
    case FETCH_CONFIG:
      return state.set('isFetching', true);

    case FETCH_CONFIG_SUCCESS:
      const res = typeof action.result === 'string'
        ? JSON.parse(action.result)
        : action.result;

      return state.merge({
        isFetching: false,
        data: Immutable.fromJS(res),
        error: null,
        lastModified: action.receivedAt,
      });

    case FETCH_CONFIG_FAILED:
      return state.merge({
        isFetching: false,
        error: action.error.message,
      });

    default:
      return state;
  }
}

export default config;
