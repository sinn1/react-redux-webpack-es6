import Immutable from 'immutable';
import {
  TOGGLE_RIGHT_SIDE_PANEL_VISIBILITY,
  TOGGLE_LEFT_SIDE_PANEL_VISIBILITY,
  HANDLE_WINDOW_RESIZE_EVENT,
  UPDATE_KEYBOARD_HEIGHT,
} from '../actions/UiActions.js';

import {
  LOAD
} from 'redux-storage';

const DefaultState = Immutable.Record({
  isRightPanelVisible: false,
  isLeftPanelVisible: false,
  windowWidth: window.innerWidth,
  windowHeight: window.innerHeight,
  rehydrated: false,
  keyboardHeight: 0,
});

const initialState = new DefaultState;

function ui(state = initialState, action) {

  function isVisible(lastState, action) {
    if (action.isVisible === undefined || typeof(action.isVisible) !== 'boolean') {
      return !lastState;
    }

    return action.isVisible;
  };

  switch (action.type) {
    case TOGGLE_RIGHT_SIDE_PANEL_VISIBILITY:
      return state.set('isRightPanelVisible', isVisible(state.get('isRightPanelVisible'), action));

    case TOGGLE_LEFT_SIDE_PANEL_VISIBILITY:
      return state.merge({
        isLeftPanelVisible: isVisible(state.get('isLeftPanelVisible'), action),
      });

    case HANDLE_WINDOW_RESIZE_EVENT:
      return state.merge({
        windowHeight: action.windowHeight,
        windowWidth: action.windowWidth,
      });

    case LOAD:
      return state.set('rehydrated', true);

    case UPDATE_KEYBOARD_HEIGHT:
      return state.set('keyboardHeight', action.height);
    default:
      return state;
  }
}

export default ui;
