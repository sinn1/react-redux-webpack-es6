// components do not need to know about stores or actions.
// components should receive props and call prop callbacks
import React, {PropTypes, Component} from 'react';
import styles from './style.css';

export default class ListOrNo extends Component {
  static propTypes = {
    listItems: PropTypes.object,
    noItemsMessage: PropTypes.string,
    loading: PropTypes.bool.isRequired,
  }

  render() {
    const list = this.props.listItems && this.props.listItems.size > 0
      ? this.props.listItems.map((x, i) => (<li key={i} className={styles.li}>{x}</li>))
      : null;

    return (
      <div>
        {
          this.props.loading
          ? <span className={styles.loading}>LOADING...</span>
          : list
            ? <ul>
                {list}
              </ul>
            : <span className={styles.noItems}>{this.props.noItemsMessage || 'no items'}</span>
        }
      </div>
    );
  }
}
