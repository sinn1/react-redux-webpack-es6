import React, { PropTypes, Component } from 'react';
import appendVendorPrefix from 'react-kit/appendVendorPrefix';

import styles from './styles.css'

export default class OffCanvasPushMenu extends Component {

  static propTypes = {
    id: PropTypes.any,
    isOpen: PropTypes.bool,
    width: PropTypes.number,
    className: PropTypes.string,
    outerContainerId: PropTypes.string,
    position: PropTypes.string,
    onTransitionEnd: PropTypes.func,
    children: PropTypes.node,
  };

  static defaultProps = {
    position: 'left',
  };

  constructor(props) {
    super(props);
    this.state = {
      isInitialRender: true,
    };


    this.menuStyle = props.position === 'left'
      ? {
        width: this.props.width,
        left: -this.props.width,
      }
      : {
        width: this.props.width,
        right: -this.props.width,
      };
  }

  componentDidMount() {
    this.applyWrapperStyles(this.props.isOpen, this.props.width);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isOpen !== nextProps.isOpen) {
      this.applyWrapperStyles(nextProps.isOpen, nextProps.width);
    } else if (nextProps.isOpen && (this.props.width !== nextProps.width)) {
      this.applyWrapperStyles(nextProps.isOpen, nextProps.width);
    }
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.width !== this.props.width
      || this.props.isOpen !== nextProps.isOpen) {
      return true;
    }

    return false;
  }

  componentWillUnmount() {
    if (this.props.onTransitionEnd) {
      const wrapper = document.getElementById(this.props.outerContainerId);
      if (wrapper) {
        const transitionEndEvent = this._transitionEndEventName();
        wrapper.removeEventListener(transitionEndEvent, this.props.onTransitionEnd);
      }
    }
  }

  _transitionEndEventName() {
    const el = document.createElement('div');
    const transitionKey = 'transition';
    const operaTransitionKey = 'OTransition';
    const mozTransitionKey = 'MozTransition';
    const webkitTransitionKey = 'WebkitTransition';

    const transitions = {
      [transitionKey]: 'transitionend',
      [operaTransitionKey]: 'otransitionend',
      [mozTransitionKey]: 'transitionend',
      [webkitTransitionKey]: 'webkitTransitionEnd',
    };

    for (const i in transitions) {
      if (transitions.hasOwnProperty(i) && el.style[i] !== undefined) {
        return transitions[i];
      }
    }
  }

  applyWrapperStyles(isOpen, width) {
    if (this.props.outerContainerId) {
      this.handleExternalWrapper(this.props.outerContainerId, isOpen, width);
    }
  }

  handleExternalWrapper(id, isOpen, width) {
    const wrapper = document.getElementById(id);

    if (!wrapper) {
      console.error("Element with ID '" + id + "' not found");
      return;
    }

    if (this.props.onTransitionEnd && this.state.isInitialRender) {
      const transitionEndEvent = this._transitionEndEventName();
      wrapper.addEventListener(transitionEndEvent, this.props.onTransitionEnd, false);
      this.setState({
        isInitialRender: false,
      });
    }

    const openTrans = this.props.position === 'left'
      ? width
      : width * -1;

    const wrapperStyles = appendVendorPrefix({
      transform: isOpen ? `translate3d(${openTrans}px, 0, 0)` : `translate3d(0,0,0)`,
      transition: 'transform 300ms ease',
    });

    for (const prop in wrapperStyles) {
      if (wrapperStyles.hasOwnProperty(prop)) {
        wrapper.style[prop] = wrapperStyles[prop];
      }
    }
  }

  render() {
    return (
      <div className={styles['offcanvas-push-menu-outer']}>
        <div className={styles['offcanvas-push-menu']}
          id={ this.props.id }
          style={ this.menuStyle }>
          <div className={styles['offcanvas-push-menu-inner']}>
            <nav className={this.props.className}>
              { this.props.children }
            </nav>
          </div>
        </div>
      </div>
    );
  }
}
