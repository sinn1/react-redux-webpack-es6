
export function save(key, value) {
   window.localStorage.setItem(key, JSON.stringify(value));
}

export function load(key) {
   const result = window.localStorage.getItem(key);
   return JSON.parse(result);
}

export function remove(key) {
   window.localStorage.removeItem(key);
}
