const endpoints = [];

export function fetch(url, { params, data, options }) {
  for (let i = 0; i < endpoints.length; i++) {
    const endpointCallback = endpoints[i];
    if (url.match(endpointCallback.matcher)) {
      console.log(`matched fetch: ${url}`);
      return endpointCallback.callback(params, data, options);
    }
  }

  return {
    error: {
      message: 'Mock not created for url: ' + url,
    }
  };
}

function registerEndpoint(urlMatcher, callback) {
  endpoints.push({
    matcher: urlMatcher,
    callback,
  });
}

registerEndpoint(/^\/api\/config/g, (params, data, options) => {
  return {
    identityServerUrl: 'someUrl',
    clientId: 'someId',
    scopes: 'scopes',
    responseType: 'someType',
    pointsNeeded: 6,
  };
});

