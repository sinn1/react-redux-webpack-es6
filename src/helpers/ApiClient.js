import fetch from 'isomorphic-fetch';
import config from '../config';

const methods = ['get', 'post'];
const acceptHeader = 'Accept';
const contentTypeHeader = 'Content-Type';
const authHeader = 'Authorization';
const contentType = 'application/json';

function formatUrl(path) {
  const adjustedPath = path[0] !== '/' ? '/' + path : path;

  return '/api' + adjustedPath;
}

function throwError(response, errorMessage) {
  let error = new Error(errorMessage);
  error.response = response;
  throw error;
}

function checkStatus(response) {
  if (response.status < 200 || response.status >= 300) {
    throwError(response, response.statusText);
  }

  return response;
}

function parseJson(response) {
  return response.json();
}

function checkValidationResult(data) {
  if (data.hasOwnProperty('success') && !data.success) {
    throwError(null, data.errorSummary);
  } else {
    return data;
  }
}

function serializeParams(obj) {
  var str = [];
  for (var p in obj) {
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  }

  return str.join("&");
}

class ApiClient {
  constructor(baseUrl, tokenGetter) {
    if (!baseUrl || baseUrl === '') {
      const windowUrl = window.location;
      this.baseUrl = windowUrl.protocol + '//' + windowUrl.host;
    } else {
      this.baseUrl = baseUrl;
    }

    methods.forEach((method) => {
      this[method] = (path, { params, data, options } = {}) => new Promise((resolve, reject) => {
        const adjustedPath = formatUrl(path);
        const token = tokenGetter ? tokenGetter() : '';
        let url = this.baseUrl + adjustedPath;

        let init = {
          method: method,
          headers: {
            [acceptHeader]: contentType,
          },
        };

        if (!options || !options.isPublicPath) {
          init.headers[authHeader] = `Bearer ${token}`;
        }

        if (params) {
          url = url + '?' + serializeParams(params);
        }

        if (data) {
          if (data instanceof FormData) {
            init.body = data;
          } else {
            init.body = JSON.stringify(data);
            init.headers[contentTypeHeader] = contentType;
          }
        }

        fetch(url, init)
          .then(checkStatus)
          .then(parseJson)
          .then(checkValidationResult)
          .then((data) => {
            resolve(data);
          }).catch((error) => {
            console.log('API_CLIENT ERROR: ' + error.message);
            reject(error);
          });
      });
    });
  }
}

export default ApiClient;
