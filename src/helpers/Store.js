import * as Storage from './LocalStorage';

const storage = Storage;

export function saveTokens(tokens) {
  var appStorage = load('app-store');
  appStorage.auth.tokens = tokens;
  save('app-store', appStorage);
}

export function retrieveTokens() {
  var appStorage = load('app-store');
  return appStorage.auth.tokens;
}

export function retrieveAccessToken() {
  const tokens = retrieveTokens();
  if (tokens) {
    return tokens.accessToken;
  }

  return null;
}

export function clearTokens() {
  var appStorage = load('app-store');
  appStorage.auth.tokens = null;
  save('app-store', appStorage);
}

export function save(key, value) {
  storage.save(key, value);
}

export function load(key) {
  const val = storage.load(key);

  if (val) {
    return val;
  }

  return null;
}

export function remove(key) {
  storage.remove(key);
}

export function calculateExpiryDate(seconds) {
  let expiryDateTime = new Date();
  expiryDateTime.setSeconds(expiryDateTime.getSeconds() + (parseInt(seconds) || 1000));
  return expiryDateTime;
}
