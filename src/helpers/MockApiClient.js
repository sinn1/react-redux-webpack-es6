import * as MockData from './MockData';

const methods = ['get', 'post'];

function formatUrl(path) {
  const adjustedPath = path[0] !== '/' ? '/' + path : path;
  return '/api' + adjustedPath;
}

class ApiClient {
  constructor() {
    methods.forEach((method) => {
      this[method] = (path, { params, data, options } = {}) => new Promise((resolve, reject) => {
        const adjustedPath = formatUrl(path);

        const mockData = MockData.fetch(adjustedPath, { params, data, options });
        if (mockData.error) {
          console.log(mockData.error.message);
          reject(error);
        } else {
          resolve(mockData);
        }
      });
    });
  }
}

export default ApiClient;
