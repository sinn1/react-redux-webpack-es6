import React, {PropTypes, Component} from 'react';
import Block from 'react-blocks';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';
import {Motion, spring} from 'react-motion';
import appendVendorPrefix from 'react-kit/appendVendorPrefix';

import { fetchCatFacts } from '../../redux/actions/CatFactActions';

import styles from './styles.css';

@connect(state => ({
  windowWidth: state.getIn(['ui', 'windowWidth']),
  windowHeight: state.getIn(['ui', 'windowHeight']),
  catFacts: state.get('catFacts'),
  config: state.get('config'),
}), { fetchCatFacts })
export default class Home extends Component {
  static propTypes = {
    windowWidth: PropTypes.number.isRequired,
    windowHeight: PropTypes.number.isRequired,
    catFacts: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
  }

  componentDidMount() {
    this.props.fetchCatFacts();
  }

  _renderList(listItems) {
    return (
      <Motion defaultStyle={{x: 0}} style={{x: spring(100)}}>
        {
          interpolatedStyles => {
            const style = {
              opacity: interpolatedStyles.x / 100,
            };

            return (
              <ul style={style}>
                { listItems.map((x, i) => (<li key={i} className={styles.li}>{x}</li>)) }
              </ul>
            );
          }
        }
      </Motion>
    );
  }

  render() {
    const catFacts = this.props.catFacts;
    const listItems = catFacts.getIn(['data', 'facts']);

    return (
      <div className={styles.home}>
        {
          listItems && listItems.size > 0
            ? this._renderList(listItems)
            : 'No Items'
        }
        <button className={styles.getMoreButton} onClick={this.props.fetchCatFacts}>Get More Facts!</button>
      </div>
    );
  }
}
