import React, { Component } from 'react';

import styles from './styles.css';

export default class AnotherView extends Component {
  render() {
    return (
      <div className={styles.anotherView}>
      	Another View
      </div>
    );
  }
}
