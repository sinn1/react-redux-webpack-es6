import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import { Router, Route, Link } from 'react-router'
import autobind from 'autobind-decorator';
import { push } from 'react-router-redux';

import {
  handleWindowResizeEvent,
  toggleLeftSidePanelVisibility,
} from '../../redux/actions/UiActions';
import { debounce } from '../../helpers/Utils';

import OffCanvasPushMenu from '../../components/OffCanvasPushMenu';

import styles from './styles.css';

@connect(
  state => ({
    isLeftPanelVisible: state.getIn(['ui', 'isLeftPanelVisible']),
    windowWidth: state.getIn(['ui', 'windowWidth']),
  }),
  { handleWindowResizeEvent, toggleLeftSidePanelVisibility, push }
)
export default class AppContainer extends Component {
  static propTypes = {
    windowWidth: PropTypes.number.isRequired,
  }

  constructor(props) {
    super(props);
    this.debouncedHandleWindowResize = debounce(this.props.handleWindowResizeEvent, 60);
  }

  componentDidMount() {
    window.addEventListener('resize', this.debouncedHandleWindowResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.debouncedHandleWindowResize);
  }

  _renderMenu() {
    return (
      <OffCanvasPushMenu
        outerContainerId={'container'}
        width={260}
        isOpen={this.props.isLeftPanelVisible}
        className={styles.sidePanel}>
        <Link to={'/'}>Home</Link>
        <Link to={'/anotherView'}>Another View</Link>
      </OffCanvasPushMenu>
    );
  }

  render() {
    return (
      <div id='container' ref='container' className={ styles['app-container'] }>
        { this._renderMenu() }
        <div className={styles.topNav}>
          <button onClick={this.props.toggleLeftSidePanelVisibility}>Menu</button>
        </div>
        <div className={styles.mainContent}>
          { this.props.children }
        </div>
      </div>
    );
  }
};
