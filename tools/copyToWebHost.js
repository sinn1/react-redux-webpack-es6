var copy = require('copy');
var constants = require('./constants');

copy(constants.DIST_DIR + '/*', constants.WEBHOST_DIR, function() {
  console.log('Copied output to WebHost directory!');
});
