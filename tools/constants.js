var path = require('path');

const ABSOLUTE_BASE = path.normalize(path.join(__dirname, '..'));

const constants = Object.freeze({
  ABSOLUTE_BASE: ABSOLUTE_BASE,
  NODE_MODULES_DIR: path.join(ABSOLUTE_BASE, 'node_modules'),
  BUILD_DIR: path.join(ABSOLUTE_BASE, 'build'),
  DIST_DIR: path.join(ABSOLUTE_BASE, 'dist'),
  WEBHOST_DIR: path.join(ABSOLUTE_BASE, '../backend/src/zindexdigital.cocotea.webapp/app'),
  PHONEGAP_DIST_DIR: path.join(ABSOLUTE_BASE, 'phonegap/www'),
  SRC_DIR: path.join(ABSOLUTE_BASE, 'src'),
  TOOLS_DIR: path.join(ABSOLUTE_BASE, 'tools'),
  HOT_RELOAD_PORT: 3000,
  AUTOPREFIXER_BROWSERS: [
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 40',
    'chrome >= 44',
    'safari >= 6',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
  ],
});

module.exports = constants;
