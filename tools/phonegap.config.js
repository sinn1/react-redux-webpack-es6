var webpack = require('webpack');
var _ = require('lodash');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

var constants = require('./constants');
var webpackConfig = require('./webpack.config');

module.exports = _.assign(webpackConfig, {
  entry: [
    constants.SRC_DIR + '/index',
    constants.SRC_DIR + '/index.gap.html',
  ],
  devtool: 'source-map',
  output: {
    path: constants.PHONEGAP_DIST_DIR,
    filename: 'bundle.js',
    publicPath: '',
  },
  plugins: [
    new CopyWebpackPlugin([
      // { from: 'src/static/cordova.js'},
      // { from: 'src/static/cordova_plugins.js'},
      // { from: 'src/static/plugins', to: 'plugins'},
    ]),
    new ExtractTextPlugin('style.css', { allChunks: true }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      __PHONEGAP__: true,
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
      __DEV__: false,
    }),
  ],
});
