//write this in nodejs compliant js
var path = require('path');
var express = require('express');
var webpack = require('webpack');
var webpackMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var request = require('request');
require('ssl-root-cas').inject();

var config = require('./webpack.config.dev');
var constants = require('./constants');

const app = express();
const builder = webpack(config);

app.use('/api', function(req, res) {
  var url = 'http://catfacts-api.appspot.com/api' + req.url;

  req.pipe(request({ 'url': url, 'rejectUnauthorized': false })).pipe(res);
  console.log(url);
});

app.use(webpackMiddleware(builder, {
  noInfo: true,
  publicPath: config.output.publicPath,
}));

app.use(webpackHotMiddleware(builder));

app.get('*', function(req, res) {
  if (req.url.indexOf('/api') === 0) {
    return;
  }

  //todo: isomorphic rendering here: https://github.com/este/este/blob/master/web/src/server/frontend/render.js
  res.sendFile(path.join(constants.SRC_DIR, 'index.html'));
});

app.listen(constants.HOT_RELOAD_PORT, 'localhost', function(err) {
  if (err) {
    console.warn(err);
    return;
  }

  console.log('server available at http://localhost:%s', constants.HOT_RELOAD_PORT);
});
