var webpack = require('webpack');
var _ = require('lodash');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var constants = require('./constants');
var webpackConfig = require('./webpack.config');

var plugins = webpackConfig.plugins;

module.exports = _.assign(webpackConfig, {
  devtool: 'source-map',
  output: {
    path: constants.DIST_DIR,
    filename: 'bundle.js',
    publicPath: '/static/',
  },
  plugins: _.concat(webpackConfig.plugins, [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
      __DEV__: false,
      __PHONEGAP__: false,
    }),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false,
      },
      screwIE8: true,
    }),
  ]),
});
