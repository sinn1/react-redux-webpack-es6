var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var _ = require('lodash');

var constants = require('./constants');
var webpackConfig = require('./webpack.config');

module.exports = _.assign(webpackConfig, {
  devtool: 'source-map',
  output: {
    path: constants.BUILD_DIR,
    filename: 'bundle.js',
    publicPath: '/static/',
  },
  plugins: _.concat(webpackConfig.plugins, [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      __DEV__: true,
      __PHONEGAP__: false,
    }),
  ]),
  node: {
    fs: 'empty',
  },
});
