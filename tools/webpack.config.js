var webpack = require('webpack');
var path = require('path');
var postCSSImport = require('postcss-import');
var postCSSNested = require('postcss-nested');
var postCSSNext = require('postcss-cssnext');
var postCSSSimpleVars = require('postcss-simple-vars');
var postCSSImport = require('postcss-import');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var strip = require('strip-loader');

var constants = require('./constants');

var DEBUG = process.env.NODE_ENV !== 'production' ? true : false;

var entry = [
  constants.SRC_DIR + '/index',
  constants.SRC_DIR + '/index.html',
];

if (DEBUG) {
  entry.push('webpack-hot-middleware/client');
}

var jsLoader = DEBUG
  ? {
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        plugins: [
          "transform-runtime",
          ["react-transform", {
            "transforms": [{
              "transform": "react-transform-hmr",
              "imports": ["react"],
              "locals": ["module"]
            }, {
              "transform": "react-transform-catch-errors",
              "imports": ["react", "redbox-react"]
            }]
          }]
        ]
      }
    }
  : { test: /\.js$/, exclude: /node_modules/, loaders: [strip.loader('debug'), 'babel-loader'] };

module.exports = {
  entry: entry,

  module: {
    loaders: [
    jsLoader
    , {
      test: /\.html$/,
      loader: "file?name=index.html",
    }, {
      test: /\.json$/,
      loader: 'json-loader',
    }, {
      test: /\.txt$/,
      loader: 'raw-loader',
    }, {
      test: /\.(png|jpg|jpeg|gif|woff|woff2)$/,
      loader: 'file-loader',
    }, {
      test: /\.(svg|eot|ttf|wav|mp3)$/,
      loader: 'file-loader',
    },
    {
      test: /\.css(\?v=\d+\.\d+\.\d+)?$/,
      include: path.resolve(__dirname, constants.SRC_DIR + '/styles/'),
      loader: DEBUG
        ? 'style-loader!css-loader'
        : ExtractTextPlugin.extract('style-loader', 'css-loader')
    },
    {
      test: /\.css(\?v=\d+\.\d+\.\d+)?$/,
      exclude: path.resolve(__dirname, constants.SRC_DIR + '/styles/'),
      loader: DEBUG
        ? 'style-loader!css-loader?modules&importLoaders=1&localIdentName=[name]_[local]_[hash:base64:5]!postcss-loader'
        : ExtractTextPlugin.extract('style-loader', 'css-loader?modules&importLoaders=1&localIdentName=[name]_[local]_[hash:base64:5]!postcss-loader')
    },
    ],
  },

  plugins: [
    new ExtractTextPlugin('[name].css'),
  ],

  postcss: function plugins(webpack) {
    return [
      postCSSImport({ addDependencyTo: webpack }),
      postCSSSimpleVars({ silent: true }),
      postCSSNested(),
      postCSSNext(),
    ];
  },

  node: {
    fs: 'empty',
  },
};
